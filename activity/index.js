console.log("Hello World");

// 1st loop
let num = Number(prompt("Number"))
for(num; num >= 50; num--){
	if (num <= 50){
		console.log("The current value is at 50. Terminating the loop.")
		break;
	}
	if (num % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number")
		continue
	}
	if (num % 5 === 0){
		console.log(num)
	}
	
}
// 2nd loop
let word = "supercalifragilisticexpialidocious";
let cons = "sprclfrglstcxpldcs";
for(i = 0; i < word.length; i++){
	if(
		word[i] == 'a' ||
		word[i] == 'e' ||
		word[i] == 'i' ||
		word[i] == 'o' ||
		word[i] == 'u' 
		){
		continue;
	} else {
		console.log(word[i]+cons)
	}
}